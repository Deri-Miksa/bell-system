import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Settings from '../views/Settings.vue'
import Nodes from '../views/Nodes.vue'
import Remote from '../views/Remote.vue'
const jwt = require("jsonwebtoken");

const publicKey = `
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzkP2+p/uvYJM9FTcf1il
r86bibJE7eFtVllGggxfAZifyBzipAVEIXy4B2SwtVAWWucNcztfTdP2zWxrOBR+
vuTFn+18l4EafLLIi2JaM2GDEdHacjGtAnVOkVCaxf24yi3Ss5VfvSLdkTF5XzL0
KGzD5NMVsmAb5miJnX9Fcx+EHmsIYYexLg7iQzkjTs8MDdhTgU9LuLOMoAfJ+oQv
bxv9LtzNKXIDhgwG6GmT7Co4UcYkIEvK1qFKd80uo0+H6HLtCSV2XXS77js6hNfH
O0jjVwZFIKHNgr8C1VMSBJFXs+/Xu50n5pZjLQuDn/OidREFNAED03dnngE2PA0i
xwIDAQAB
-----END PUBLIC KEY-----
`;

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings
  },
  {
    path: '/nodes',
    name: 'Nodes',
    component: Nodes
  },
  {
    path: '/remote',
    name: 'Remote',
    component: Remote
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.name != "Home"){
    try{
      if (!localStorage.Authorization) return next('/');
      var token = localStorage.Authorization;
      token = token.slice(7, token.length);
      jwt.verify(token, publicKey, function(err, decoded) { // decode jwt token
        if (!err) next();
        else next('/');
      });
    } catch(e){
      next('/')
    }
    next('/');
  }
});

export default router
