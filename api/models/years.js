const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    temperature: {
      type: Number
    },
    humidity: {
      type: Number
    },
    airquality: {
      type: Number
    },
    id:{
      type: String
    },
  date: {
    type: Date
  }
});

// export model user with UserSchema
module.exports = mongoose.model("year", UserSchema);
