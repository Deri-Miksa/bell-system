const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    name:{
      type: String
    },
    nodeId:{
      type: String
    },
    type:{
      type: String
    },
    location:{
      type: String
    },
    date: {
      type: Date
    }
});

// export model user with UserSchema
module.exports = mongoose.model("node", UserSchema);
