const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    ssid: {
      type: String
    },
    rssi: {
      type: Number
    },
    ip: {
      type: String
    },
    gateway: {
      type: String
    },
    temperature: {
      type: Number
    },
    uptime: {
      type: Number
    },
    bootcount: {
      type: Number
    },
    id:{
      type: String
    },
    type:{
      type: String
    },
  date: {
    type: Date
  }
});

// export model user with UserSchema
module.exports = mongoose.model("telemetry", UserSchema);
