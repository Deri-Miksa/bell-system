const mongoose = require("mongoose");

const RevokeSchema = mongoose.Schema({
  id: {
    type: String,
    required: true
  },
  token: {
    type: String,
    required: true
  },
  expDate: {
    type: Date,
    required: true
  },
  revokedAt: {
    type: Date,
    required: true
  }
});

// export model user with UserSchema
module.exports = mongoose.model("revoke", RevokeSchema);
