# Bell-system

API végpontok / API endpoints:

/device
    /new
    /delete
    /all
    /trigger
    /metrics

/user
    /info (GET)
        - Hitelesített felhasználó: token
        - RES: A felhasználó összes tárolt adata
    /pass/:data (PATCH)
        - Hitelesített felhasználó: token
        - Régi jelszó: oldPass
        - Új jelszó: newPass
        - RES: {status: "ok"}
        - Fun: Régi jelszó lecserélése
    /email/:data (PATCH)
        - Hitelesített felhasználó: token
        - RES: {status: "ok"}
        - Fun: Régi email cím lecserélése
    /:data (POST)
        - Hitelesített felhasználó: token
        - Felhasználónév: username
        - Email: email
        - Jelszó: password
        - RES: {status: "ok"}
        - Fun: Új felhasználó létrehozása
    /auth (GET)
        - Hitelesített felhasználó: token
        - RES: {status: "ok"}
        - Fun: Auth middleware tesztelése
    /auth (POST)
        - Email: email
        - Jelszó: password
        - RES: {status: "ok"}
        - Fun: Felhasználói token generálása
    /auth (DELETE)
        - Hitelesített felhasználó: token
        - RES: {status: "ok"}
        - Fun: Felhasználói token rögzítése az adatbázisban

/status (GET)
  - Healthcheck API
  - RES: {status: "ok"}
