  const jwt = require("jsonwebtoken");
require('dotenv').config()
const Revoke = require("../models/jwtrevoke");
const key = require('../config/public');
var msgpack = require("msgpack-lite");

module.exports = function(req, res, next) {
  let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
  try {
    if (token.startsWith('Bearer ')) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }
    if (!token) {
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason: 'Hiányzó token!'}).toString('hex'))
    }

    jwt.verify(token, key.publicKey, function(err, decoded) { // decode jwt token
      if (!err){
        Revoke.find({ token: token }, function(err, data) { // searching current token in revoked token section
          if (data.length) {
            // If token revoked, and found in DB, return unauthenticated state.
            // If someone stole the JWT token before crypto keys expired, but user logged out, this branch mitigate to reunauthenticate with JWT.
            // Try harder hacker boi! :-D
            res
            .status(200)
            .send(msgpack.encode({status: 'error', reason: 'Already logged out!'}).toString('hex'))
          } else {
            req.id = decoded.user.id;
            req.role = decoded.user.role;
            req.exp = decoded.exp;
            next();
          }
        });
      } else {
        return res
        .status(200)
        .send(msgpack.encode({status: 'error', reason: err.message}).toString('hex'))
      }
    });

    //if token is not valid, catch branch catch exception and return unauthenticated state
  } catch (e) {
    console.log("Auth middleware error:", e);
    return res
    .status(500)
    .send(msgpack.encode({status: 'error'}).toString('hex'))
  }
};
