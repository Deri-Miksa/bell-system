var express = require('express');
var router = express.Router();
require('dotenv').config();
const auth = require("../middleware/auth");
const { SecurePass } = require('argon2-pass'); //Password hash package
var validate = require("validate.js");
const jwt = require("jsonwebtoken"); //JWT: token generator and verify
const key = require('../config/private');
var msgpack = require("msgpack-lite");
const dayjs = require('dayjs');
const crypto = require('crypto')

const Revoke = require("../models/jwtrevoke");
const User = require('../models/user')


  var constraints = {
    email: {
      email: {
        message: 'nem érvényes!'
      }
    },
    password:{
      length: {
        minimum: 6,
        tooShort: 'nem elég hosszú!'
      }
    }
  };

//Get all info by user id.
router.get('/info', auth, async (req, res, next) => {
    let user = await User.findOne({_id: req.id});
    return res
    .status(200)
    .send(msgpack.encode({status: 'ok', email: user.email, username: user.username, createdAt: user.createdAt, avatar: "https://www.gravatar.com/avatar/" + crypto.createHash('md5').update(user.email).digest("hex") + ".jpg"}).toString('hex'));
});

//Change current user password
router.patch('/pass/:data', auth, async (req, res, next) => {
    try {
      let user = await User.findOne({_id: req.id});
      var { oldPass, newPass } = msgpack.decode(Buffer.from(req.params.data, "hex"));



      if (oldPass == newPass) {
        return res
        .status(200)
        .send(msgpack.encode({status: 'error', reason:  'Nem adható meg ugyan az a jelszó!'}).toString('hex'));
      }

      const sp = new SecurePass();
      const result = await sp.verifyHash(Buffer.from(oldPass), user.password);

      if (SecurePass.isInvalid(result)) {

        return res
        .status(200)
        .send(msgpack.encode({status: 'error', reason:  'Helytelen a régi jelszó!'}).toString('hex')); //Password incorrect, send back this reason

      } else if (SecurePass.isValid(result)) {
        newPass = await sp.hashPassword(Buffer.from(newPass));
        await User.findOneAndUpdate({_id: req.id},{password: newPass});

        return res
        .status(200)
        .send(msgpack.encode({status: 'ok'}).toString('hex'));
      }
    } catch(e){
      console.log('Change pass error:',e);
      return res
      .status(500)
      .send(msgpack.encode({status: 'error'}).toString('hex'));
    }
});

router.patch('/email/:data', auth, async (req, res, next) => {
  try {
    const { newEmail } = msgpack.decode(Buffer.from(req.params.data, "hex"));
    var uniqueEmail = await User.findOne({email: newEmail});
    if (uniqueEmail) {
      await User.findOneAndUpdate({_id: req.id},{email: newEmail});

      return res
      .status(200)
      .send(msgpack.encode({status: 'ok'}).toString('hex'));
    }
    else {
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason: 'Email cím már használva van!'}).toString('hex'));
    }
  } catch (e) {
    console.log('Change email error:', e);
    return res
    .status(500)
    .send(msgpack.encode({status: 'error'}).toString('hex'));
  }
});

//Create a new user
router.post("/:data", auth, async (req, res, next) => {
  try {
    const { password, email, username, role } = msgpack.decode(Buffer.from(req.params.data, "hex"));

    var ssvEmail = validate({email: email}, constraints);
    if (ssvEmail) {
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason:  ssvEmail.email[0]}).toString('hex'));
    }

    var ssvPassword = validate({password: password}, constraints);
    if (ssvPassword){
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason:  ssvPassword.password[0]}).toString('hex'));
    }

    var ssvUsername = validate({username: username}, { username: { presence: { allowEmpty: false, message: 'nem lehet üres!' } } });
    if (ssvUsername) {
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason: ssvUsername.username[0]}).toString('hex'));
    }

    // Already user searching in DB
    let user = await User.findOne({ $or: [
        {email: email},
        {username: username}
    ]
    });

    if (user) {
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason: 'Felhasználó már létezik!'}).toString('hex'));  //User is already exist, so can't overwrite present value
    }

    const sp = new SecurePass();
    const hash_password = await sp.hashPassword(Buffer.from(password));

    var date = Date.now();
    user = new User({
      email,
      hash_password,
      role,
      username,
      date
    });

    user.email = email;
    user.password = hash_password;

    await user.save(); //Waitnig for saving the data to DB

    return res
    .status(200)
    .send(msgpack.encode({status: "ok"}).toString('hex'));
  } catch (e) {
    console.log('User create error:', e);
    //Catch the error and send back
    return res
    .status(500)
    .json({status: 'error'});
  }
});

//Generate jwt token and validate user by email and password
router.post("/auth/:data", async (req, res) => {
  try {
    const { password, email } = msgpack.decode(Buffer.from(req.params.data, "hex"));

    var ssvEmail = validate({email: email}, constraints);
    if (ssvEmail){
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason:  ssvEmail.email[0]}).toString('hex'));
    }
    var ssvPassword = validate({password: password}, constraints);
    if (ssvPassword){
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason:  ssvPassword.password[0]}).toString('hex'));
    }

    let user = await User.findOne({
      email
    }); //Get user by email from DB

    if (!user) {
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason:  'Nincs ilyen felhasználó!'}).toString('hex')); //User is not exist, so can't login
    }

    const sp = new SecurePass();
    const result = await sp.verifyHash(Buffer.from(password), user.password);

    if (SecurePass.isInvalidOrUnrecognized(result)) {

      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason:  'Not secure Password Hash or invalid! Please connect to webmaster!'}).toString('hex')); //Password incorrect, send back this reason

    } else if (SecurePass.isInvalid(result)) {

      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason:  'Helytelen jelszó!'}).toString('hex')); //Password incorrect, send back this reason

    } else if (SecurePass.isValid(result)) {

      const payload = {
        user: {
          id: user.id,
          role: user.role
        }
      };

      //Generate JWT token for later authentication
      //Require a private key to sign this token
      //Can be set the expires date
      jwt.sign(payload, key.privateKey, {
        algorithm: 'RS256',
        expiresIn: '1h'
      }, (err, token) => {
        if (err) throw err;

        return res
        .status(200)
        .send(msgpack.encode({"status": "ok", "token": "Bearer " + token}).toString('hex'));

      });

    } else if (SecurePass.isValidNeedsRehash(result)) {

      await User.findOneAndUpdate( { 'email': email } , { password: await sp.hashPassword(Buffer.from(password)) } , {upsert: true}, function(err, doc) {
        if (err) console.log("Rehashing error: ", err);
      });

      const payload = {
        user: {
          id: user.id,
          role: user.role
        }
      };

      //Generate JWT token for later authentication
      //Require a private key to sign this token
      //Can be set the expires date
      jwt.sign(payload, key.privateKey, {
        algorithm: 'RS256',
        expiresIn: '10m'
      }, (err, token) => {
        if (err) throw err;

        return res
        .status(200)
        .send(msgpack.encode({status: 'ok', token: 'Bearer ' + token}).toString('hex'));
      });
    }
  } catch (e) {
    console.log('JWT gen and auth error:', e);
    //Catch the error and send back
    return res
    .status(500)
    .send(msgpack.encode({status: 'error'}).toString('hex'));
  }
});

//Add the current jwt token to revoked db
router.delete('/auth', auth, async (req, res, next) => {
  try {

    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    if (token.startsWith('Bearer ')) {
      // Remove Bearer from string
      token = token.slice(7, token.length);
    }

    var id = req.id;
    var expDate = dayjs(req.exp).format();
    var revokedAt = dayjs().format();

    revoke = new Revoke({
      id,
      token,
      expDate,
      revokedAt
    });

    revoke.save();

    return res
    .status(200)
    .send(msgpack.encode({status: 'ok'}).toString('hex'));
  } catch(e){
    console.log('Logout error:', e);
    return res
    .status(500)
    .send(msgpack.encode({status: 'error'}).toString('hex'));
  }

});

//Check auth state
router.get('/auth', auth, async (req, res, next) => {
  return res
  .status(200)
  .send(msgpack.encode({status: 'ok'}).toString('hex'));
});

//Get all user from database
router.get('/all', auth, async (req, res, next) => {
  try {
    var users = await User.find({});
    var allUser = new Array();
    if (users.length == 0) {
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason: 'Nincsen eszköz az adatbázisban'}).toString('hex'));
    } else {
      for (var i = 0; i < users.length; i++) {
        allUser[i] = new Object();
        allUser[i].id = users[i].id;
        allUser[i].username = users[i].username;
        allUser[i].avatar = "https://www.gravatar.com/avatar/" + crypto.createHash('md5').update(users[i].email).digest("hex") + ".jpg";
      }

      return res
      .status(200)
      .send(msgpack.encode(allUser).toString('hex'));
    }
  } catch (e) {
    console.log('Get all user error:', e);

    return res
    .status(200)
    .send(msgpack.encode({status: 'error'}).toString('hex'));
  }
});

//Delete specific user
router.delete('/:id', async (req, res, next) => {
    try {
      const { id } = msgpack.decode(Buffer.from(req.params.id, "hex"));
      if (id){
        await User.findOneAndRemove({_id: id});

        return res
        .status(200)
        .send(msgpack.encode({status: 'ok'}).toString('hex'));
      } else {
        res
        .status(200)
        .send(msgpack.encode({status: 'error', reason: 'id is required!'}).toString('hex'));
      }
    } catch (e) {
      console.log('User delete error:',e);

      return res
      .status(500)
      .send(msgpack.encode({status: 'error'}).toString('hex'));
    }
});


module.exports = router;
