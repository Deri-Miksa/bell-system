var express = require('express');
var moment = require('moment');
var router = express.Router();
const auth = require("../middleware/auth");


const Today = require("../models/today");
const Months = require("../models/months");
const Years = require("../models/years");
const Telemetry = require("../models/telemetry");
const Nodes = require("../models/nodes");


var moment = require('moment');

/* GET home page. */
router.get('/', auth, async (req, res) => {

      var payload = {};
      payload["chartData"] = new Object();
      payload["chartData"]["labels"] = new Array();
      payload["chartData"]["thisWeek"] = new Array();

      for (var i = 6; i >= 0; i--) {
        payload["chartData"]["labels"][Math.abs(Math.abs((i-1)-6)-1)] = moment(moment().day(moment().days()-i)).format('dddd');
      }

      for (var i = 0; i < 7; i++) {
        payload["chartData"]["thisWeek"][i] = Math.floor(Math.random() * Math.floor(20));
      }
      /*
      let devices = await Nodes.find({});
      devices.forEach(function(iter){
          Today.aggregate({
              "$group": {
              "id": iter.id,
              "avg_temp": { "$avg": "$temperature" }
              }
        });
      });

      */

      res.io.on('connection', function(socket){
       socket.emit('temp', payload);
     });
});

module.exports = router;
