require('dotenv').config();
var express = require('express');
var router = express.Router();
const auth = require("../middleware/auth");
const Nodes = require("../models/nodes");
var msgpack = require("msgpack-lite");
var dayjs = require('dayjs');
var mqtt = require('mqtt')

var client  = mqtt.connect("mqtt://" + process.env.BROKER_IP, {clientId:"website",username:process.env.MQTT_USER,password:process.env.MQTT_PASS,port:process.env.MQTT_PORT,clean:true})

router.patch("/:data", auth, async (req, res) => {
  try {
    var { name, location, type } = msgpack.decode(Buffer.from(req.params.data, "hex"));

    let data = await Nodes.find({}, {nodeId: 1, _id:0}).sort({nodeId:-1}).limit(1);
    var nodeId;

    if (!data.length) nodeId = '1';
    else nodeId = (Number(data[0].nodeId) + 1).toString();

    var date = Date.now();

    nodes = new Nodes({
      name,
      nodeId,
      type,
      location,
      date
    });

    await nodes.save(); //Waitnig for saving the data to DB

    let query = await Nodes.findOne({nodeId: nodeId});

    return res
    .status(200)
    .send(msgpack.encode({ _id: query._id, nodeId: nodeId, status: "ok"}).toString('hex'));

  } catch (e) {
    console.log('New node error:', e);

    return res
    .status(200)
    .send(msgpack.encode({status: 'error'}).toString('hex'));
  }
});

router.delete("/:data", auth, async (req, res) => {
  try {
    const {_id} = msgpack.decode(Buffer.from(req.params.data, "hex"));

    let data = await Nodes.findOne({_id: _id});

    if (!data){
      return res
      .status(200)
      .send(msgpack.encode({ status: "error", reason: "Nem létező eszköz"}).toString('hex'));
    } else {
      await Nodes.findOneAndRemove({_id: _id});
      return res
      .status(200)
      .send(msgpack.encode({ status: "ok"}).toString('hex'));
    }
  } catch (e) {
    console.log('Delete node error:', e);

    return res
    .status(200)
    .send(msgpack.encode({status: 'error'}).toString('hex'));
  }
});

router.get("/all", auth, async (req, res) => {
  try {
    let nodes = await Nodes.find({});
    var allNodes = new Array();
    if (nodes == 0){
      return res
      .status(200)
      .send(msgpack.encode({status: 'error', reason: 'Nincsen mentett eszköz'}).toString('hex'));
    }
    for (var i = 0; i < nodes.length; i++) {
      allNodes[i] = new Object();
      allNodes[i]._id = (nodes[i]._id).toString();
      allNodes[i].nodeId = nodes[i].nodeId;
      allNodes[i].name = nodes[i].name;
      allNodes[i].type = nodes[i].type;
      allNodes[i].location = nodes[i].location;
      allNodes[i].date = dayjs(nodes[i].date).format('YYYY-MM-DD HH:mm');
    }
    return res
    .status(200)
    .send(msgpack.encode(allNodes).toString('hex'));
  } catch (e) {
    console.log('Get all node error:', e);

    return res
    .status(200)
    .send(msgpack.encode({status: 'error'}).toString('hex'));
  }
});

router.post("/trigger/:data", auth, async (req, res) => {
  try {
    const commandReq = msgpack.decode(Buffer.from(req.params.data, "hex"));
    //{"dev": {"id": "id","type": "ESP32"},"req":{"mode": 100}}
    const modes = {
      bell: 101,
      alarm: 102,
      reset: 111,
      off: 100
    };

    var query = new Array();

    for (var i = 0; i < commandReq.nodes.length; i++) {
      query[i] = new Object();
      query[i].dev = new Object();
      query[i].dev.id = commandReq.nodes[i].id;
      query[i].dev.type = commandReq.nodes[i].type;
      query[i].req = new Object();
      query[i].req.mode = modes[commandReq.mode];
      console.log(query);
      await client.publish('request', JSON.stringify(query[i]))
    }



    return res
    .status(200)
    .send(msgpack.encode({ status: "ok"}).toString('hex'));
  } catch (e) {
    console.log('Trigger error:', e);

    return res
    .status(200)
    .send(msgpack.encode({status: 'error'}).toString('hex'));
  }
});




module.exports = router;
