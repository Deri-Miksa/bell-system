var schedule = require('node-schedule');
const Revoke = require("../models/jwtrevoke");

const Today = require("../models/today");
const Months = require("../models/months");
const Years = require("../models/years");
const Telemetry = require("../models/telemetry");

var moment = require('moment');

schedule.scheduleJob('*/15 * * * *', function() {
  try {
    const now = new Date();
    Revoke.find({}, function(err, data) {
      data.forEach(function(iter) {
        if (now > iter.expDate) {
          Revoke.findOneAndRemove({
            _id: iter._id
          }, function(err) {
            if (err) return console.log("Cleanup cron error: ", err);
          });
        }
      });
    });
    console.log("\n [T] Date:" , new Date());
    console.log(" [✓] CRON MSG: ", "Expired and revoked JWT tokens cleanup successful!");
    console.log(" ---");

  } catch (e) {
    console.log("\n [T] Date:" , new Date());
    console.log(" [!] CRON MSG: ", "Something happened to the JWT token clean CRON! Reason:", e);
    console.log(" ---");
    throw e;
  }
});

schedule.scheduleJob('10 00 * * 1', async function() {
  // Every monday 00:10: Deleted telemetry data from two weeks ago
  await Telemetry.deleteMany({
    date: {
      $gte: moment().subtract(14, 'days').startOf('day'),
      $lt: moment().subtract(7, 'days').endOf('day')
    }
  });

  console.log("\n [T] Date:" , new Date());
  console.log(" [✓] CRON MSG: ", "Deleted telemetry data from two weeks ago.");
  console.log(" ---");

});

schedule.scheduleJob('01 00 * * *', async function() {
  // Every day 00:01: yesterday data AVG --> MONTH
  // Really ???? https://docs.mongodb.com/manual/reference/operator/aggregation/avg/
  // "It was at this moment that he knew he fucked up :-("

  try {
    var aggregated = [];

    let data = await Today.find({
      date: {
        $gte: moment().subtract(1, 'days').startOf('day'),
        $lt: moment().subtract(1, 'days').endOf('day')
      }
    });
    data.forEach(function(iter) {
      if (aggregated[iter.id] == null) {
        aggregated[iter.id] = {};
        aggregated[iter.id]["ID"] = iter.id;
        aggregated[iter.id]["TEMP_SUM"] = 0;
        aggregated[iter.id]["TEMP_COUNT"] = 0;
        aggregated[iter.id]["TEMP_AVG"] = 0;

        aggregated[iter.id]["AIR_SUM"] = 0;
        aggregated[iter.id]["AIR_COUNT"] = 0;
        aggregated[iter.id]["AIR_AVG"] = 0;

        aggregated[iter.id]["HUM_SUM"] = 0;
        aggregated[iter.id]["HUM_COUNT"] = 0;
        aggregated[iter.id]["HUM_AVG"] = 0;
      }

      aggregated[iter.id]["TEMP_SUM"] += iter.temperature;
      aggregated[iter.id]["TEMP_COUNT"]++;
      aggregated[iter.id]["TEMP_AVG"] = aggregated[iter.id]["TEMP_SUM"] / aggregated[iter.id]["TEMP_COUNT"];

      aggregated[iter.id]["AIR_SUM"] += iter.airquality;
      aggregated[iter.id]["AIR_COUNT"]++;
      aggregated[iter.id]["AIR_AVG"] = aggregated[iter.id]["AIR_SUM"] / aggregated[iter.id]["AIR_COUNT"];

      aggregated[iter.id]["HUM_SUM"] += iter.humidity;
      aggregated[iter.id]["HUM_COUNT"]++;
      aggregated[iter.id]["HUM_AVG"] = aggregated[iter.id]["HUM_SUM"] / aggregated[iter.id]["HUM_COUNT"];

    });
    aggregated.forEach(async function(iter) {
      var temperature = iter["TEMP_AVG"];
      var humidity = iter["HUM_AVG"];
      var airquality = iter["AIR_AVG"];
      var id = iter["ID"];
      var date = moment().subtract(1, 'days').endOf('day');

      months = new Months({
        temperature,
        humidity,
        airquality,
        id,
        date
      });

      await months.save();
    });

    await Today.deleteMany({
      date: {
        $gte: moment().subtract(1, 'days').startOf('day'),
        $lt: moment().subtract(1, 'days').endOf('day')
      }
    });

    console.log("\n [T] Date:" , new Date());
    console.log(" [✓] CRON MSG: ", "Yesterday sensor data is aggregated and moved to week collection.");
    console.log(" ---");
  } catch (e) {
    console.log("\n [T] Date:" , new Date());
    console.log(" [!] CRON MSG: ", "Daily aggregation failed! Reason: ", e);
    console.log(" ---");
    
    throw e;
  }


});

schedule.scheduleJob('05 00 1 * *', function() {
  // Every first day in month 00:02: last MONTH AVG --> YEAR
  console.log("Sensors data aggregated to month");
});
