const MQTT = require("async-mqtt");

var options = {
  clientId: process.env.MQTT_CLIENT_ID,
  username: process.env.MQTT_USERNAME,
  password: process.env.MQTT_PASSWORD,
  port: process.env.MQTT_PORT,
  clean: true
};

async function InitiateMqttClient() {
  const client = await MQTT.connectAsync(process.env.MQTT_URL, options)
  try {
    await client.subscribe("telemetry");
    		client.on('message', (topic, message) => console.log());

    console.log("Connected to MQTT !!");
  } catch (e) {
    console.log(e);
    throw e;
  }
}

module.exports = InitiateMqttClient;
