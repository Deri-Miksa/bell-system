var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var compression = require('compression'); //Compression for http
var cors = require('cors')

var deviceRouter = require('./routes/device');
var statusRouter = require('./routes/status');
var userRouter = require('./routes/user');

var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

// view engine setup
app.use(cors())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(function(req, res, next){
  res.io = io;
  next();
});
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(compression()); //use compression
app.use(express.static(path.join(__dirname, 'public')));

// Don't forget define Router on the top
app.use('/device', deviceRouter);
app.use('/status', statusRouter);
app.use('/user', userRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.status(404).json({status: 'error', reason: 'Try it harder ;-D '});
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({error: err.message});
});


module.exports = {app: app, server: server};
