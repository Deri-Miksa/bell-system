$(document).ready(function() {
        //if submit button is clicked
        $('#submit').click(function() {
          document.getElementById("qrcode").innerHTML = "";
          var name = $("#name").val().trim();
          var location = $("#location").val().trim();
          var type;

          if (document.getElementById('esp32').checked) {
            type = "ESP32"
          }
          else if (document.getElementById('esp8266').checked){
            type = "ESP8266"
          }

          if (name !== "" && location !== ""){
            $.ajax({
              url:'config/new',
              type:'post',
              data:{name:name,location:location,type:type},
              success:function(response){
                  if (response.success){
                    document.getElementById("deviceId").value = response.deviceId;
                    document.getElementById("dbId").value = response.dbId;
                    new QRCode(document.getElementById("qrcode"), {
                        text: "{deviceId:" + response.deviceId + ",dbId:" + response.dbId + " }",
                        width: 128,
                        height: 128,
                        colorDark : "#000000",
                        colorLight : "#ffffff",
                        correctLevel : QRCode.CorrectLevel.H
                    });
                    $('#generatedDeviceData').modal('show');
                    document.getElementById("form-regdevice").reset();
                  }
              }
          });
          }
        });

        $('#delete').click(function() {
          var delID = $("#delID").val().trim();
          if ( delID !== ""){
            $.ajax({
              url:'config/delete',
              type:'post',
              data:{databaseId: delID},
              success:function(response){
                if (response.found){
                  $('#confirmDelete').modal('show');
                }
                else {
                  $('.toast').toast('show');
                }
              }
            });
          }
        });

        $('#confirm').click(function() {
          var delID = $("#delID").val().trim();
          if ( delID !== ""){
            $.ajax({
              url:'config/delete',
              type:'post',
              data:{databaseId: delID, confirm: true},
              success:function(response){
                if(response.success){
                  document.getElementById("form-deldevice").reset();
                  $('#confirmDelete').modal('hide');
                }
              }
            });
          }
        });
});
