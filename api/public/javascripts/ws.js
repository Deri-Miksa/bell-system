$("#loadingMessage").html('<img src="/images/giphy.gif" alt="" srcset="">');
var socket = io();

console.log('WS running');
console.log('Reading metrics from socket...');

socket.on('temp', function(payload){
  var data = [];
  console.log(payload);
  data.push(payload.chartData.thisWeek);
  data.push(payload.chartData.lastWeek);
  var labels = payload.chartData.labels;
  renderChart(data, labels);
});
$("#loadingMessage").html("");
