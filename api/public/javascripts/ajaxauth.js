$('#submit').click(function () {
  $.ajax({
    url: '/signin',
    type: 'POST',
    cache: false,
    data: {
      email: $('#inputEmail').val(),
      password: $('#inputPassword').val(),
    },

    success: function () {
      $('#error-group').css('display', 'none');
      alert('Your submission was successful');
    },

    error: function (data) {
      $('#error-group').css('display', 'block');
      console.log(JSON.stringify(data.responseText, null, 2))
      var errors = JSON.parse(data.responseText);
      var errorsContainer = $('#errors');
      errorsContainer.innerHTML = '';
      var errorsList = '';

      for (var i = 0; i < errors.length; i++) {
        errorsList += '<li>' + errors[i].msg + '</li>';
      }
      errorsContainer.html(errorsList);
    }
  });
});
