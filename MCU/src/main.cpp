#include <WiFi.h>
extern "C" {
	#include "freertos/FreeRTOS.h"
	#include "freertos/timers.h"
	uint8_t temprature_sens_read();
}
#include <AsyncMqttClient.h>
#include <config.h>
#include <gpio.h>
#include <ArduinoJson.h>

AsyncMqttClient mqttClient;
TimerHandle_t mqttReconnectTimer;
TimerHandle_t wifiReconnectTimer;

bool debug = true;
//Every 5 minutes recalibrate time with external reference NTP server
int current_state_ntp = 0;
int refresh_interval_ntp = 350000;

//Every 2 minutes send telemetry data to server
int last_probe_telemetry = 0;
int probe_interval_telemetry = 120000;

//Every 1 minutes send sensors data to server
int last_probe_sensor = 0;
int probe_interval_sensor = 10000;

int last_bell = 0;
int bell_interval = 2000;
bool bellStart = false;
bool bellFirst = true;

RTC_DATA_ATTR int bootCount = 0;

void connectToWifi() {
  Serial.println("Connecting to Wi-Fi...");
  WiFi.begin(WIFI_SSID, WIFI_PASS);
}

void connectToMqtt() {
  Serial.println("Connecting to MQTT...");
  mqttClient.connect();
}

void WiFiEvent(WiFiEvent_t event) {
    Serial.printf("[WiFi-event] event: %d\n", event);
    switch(event) {
    case SYSTEM_EVENT_STA_GOT_IP:
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
        connectToMqtt();
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        Serial.println("WiFi lost connection");
        xTimerStop(mqttReconnectTimer, 0); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
		xTimerStart(wifiReconnectTimer, 0);
        break;
    }
}

void onMqttConnect(bool sessionPresent) {
  Serial.println("Connected to MQTT.");
	mqttClient.subscribe(REQUEST_SUB, 2);
  mqttClient.subscribe(RESPONSE_SUB, 2);
  mqttClient.subscribe(TELEMETRY_SUB, 0);
}

void isBell()
{
  if (bellStart)
  {
    if (bellFirst)
    {
      last_bell = millis();
      bellFirst = false;
      digitalWrite(buzzerPin, HIGH);
    }
    else if (millis()>(last_bell+bell_interval))
    {
      digitalWrite(buzzerPin, LOW);
      bellFirst = true;
      bellStart = false;
    }
  }
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  Serial.println("Disconnected from MQTT.");

  if (WiFi.isConnected()) {
    xTimerStart(mqttReconnectTimer, 0);
  }
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {
}

void onMqttUnsubscribe(uint16_t packetId) {

}

void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  //topic,properties.qos,properties.dup,properties.retain, len, index, total
  if (String(REQUEST_SUB).equals(String(topic)))
  {
    const size_t capacity = 2*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + 60;
    DynamicJsonDocument doc(capacity);

    deserializeJson(doc,payload);

		const char* dev_id = doc["dev"]["id"]; // "id"
		const char* dev_type = doc["dev"]["type"]; // "ESP32"
		String id_s((const __FlashStringHelper*) dev_id);
		String node_s((const __FlashStringHelper*) NODE);

		if (id_s.equals(node_s)){
			int req_mode = doc["req"]["mode"]; // "bell"
			switch (req_mode) {
				case 101:
					bellStart = true;
					Serial.println("bell command");
					break;
				case 102:
					digitalWrite(buzzerPin,HIGH);
					Serial.println("alarm command");
					break;
	      case 111:
					Serial.println("reset command");
					ESP.restart();
				  break;
				case 100:
					Serial.println("off command");
					digitalWrite(buzzerPin,LOW);
					break;
			}
		}
  }
}

void onMqttPublish(uint16_t packetId) {

}

void isSendTelemetry()
{
  if (millis()>(last_probe_telemetry+probe_interval_telemetry))
  {
    const size_t capacity = 2*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(11);
    DynamicJsonDocument doc(capacity);

    JsonObject device = doc.createNestedObject("device");
    device["id"] = NODE;
    device["type"] = NODE_TYPE;

    JsonObject telemetry = doc.createNestedObject("telemetry");
    telemetry["ssid"] = WiFi.SSID();
		telemetry["rssi"] = String(WiFi.RSSI());
    telemetry["ip"] = WiFi.localIP().toString();
    telemetry["gateway"] =  WiFi.gatewayIP().toString();
    telemetry["temp"] = String((temprature_sens_read() - 32) / 1.8);
    telemetry["uptime"] = String(millis()/1000);
		telemetry["bootcount"] = String(bootCount);

		//TODO:
		//ESP.getSketchMD5()
		//ESP.getFlashChipSize()
		//ESP.getFlashChipSpeed()
		//ESP.getFreeHeap()
		//ESP.getHeapSize()
		//ESP.getMaxAllocHeap()
		//ESP.getMaxAllocPsram()
		//ESP.getSdkVersion()
		//ESP.getSketchSize()

		char send[5000];//IDEA: TE, ezért nem megy át az üzenet!!!!!!

    serializeJson(doc, send);
    mqttClient.publish("telemetry", 0, true, send);
    last_probe_telemetry = millis();
  }
}

void isSendSensor()
{
	if (millis()>(last_probe_sensor+probe_interval_sensor))
  {
		const size_t capacity = 2*JSON_OBJECT_SIZE(3);
		DynamicJsonDocument doc(capacity);

		JsonObject device = doc.createNestedObject("device");
		device["id"] = NODE;
		device["type"] = NODE_TYPE;

		JsonObject m = doc.createNestedObject("m");
		m["temp"] = ((temprature_sens_read() - 32) / 1.8);
		m["hum"] = 20;
		m["air"] = 10;

		char send[5000];//IDEA: TE, ezért nem megy át az üzenet!!!!!!

    serializeJson(doc, send);
    mqttClient.publish("sensor", 0, true, send);

    last_probe_sensor = millis();
  }
}

void setup() {
	++bootCount;
  if (debug)
  {
      Serial.begin(9600);
  }
  pinMode(buzzerPin, OUTPUT);
  Serial.println();
  Serial.println();

  mqttReconnectTimer = xTimerCreate("mqttTimer", pdMS_TO_TICKS(2000), pdFALSE, (void*)0, reinterpret_cast<TimerCallbackFunction_t>(connectToMqtt));
  wifiReconnectTimer = xTimerCreate("wifiTimer", pdMS_TO_TICKS(2000), pdFALSE, (void*)0, reinterpret_cast<TimerCallbackFunction_t>(connectToWifi));

  WiFi.onEvent(WiFiEvent);

  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onUnsubscribe(onMqttUnsubscribe);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.onPublish(onMqttPublish);
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
	mqttClient.setCredentials(MQTT_USER, MQTT_PASS);
	mqttClient.setMaxTopicLength(2048); //Ha gond lenne az topic hosszával
  connectToWifi();
}



void loop() {
	isSendTelemetry();
  isBell();
	isSendSensor();
}
