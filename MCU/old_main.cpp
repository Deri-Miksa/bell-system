#include <Arduino.h>
#include <NTPClient.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include <config.h>
#include <gpio.h>


extern "C"
{
	#include "freertos/FreeRTOS.h"
	#include "freertos/timers.h"
	uint8_t temprature_sens_read();

}
uint8_t temprature_sens_read();
#include <AsyncMqttClient.h>
// https://github.com/marvinroger/async-mqtt-client/blob/master/docs/2.-API-reference.md



AsyncMqttClient mqttClient;
TimerHandle_t mqttReconnectTimer;
TimerHandle_t wifiReconnectTimer;



bool debug = true;

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "europe.pool.ntp.org", 3600, 60000);

//Every 5 minutes recalibrate time with external reference NTP server
int current_state_ntp = 0;
int refresh_interval_ntp = 350000;

//Every 2 minutes send telemetry data to server
int last_probe_telemetry = 0;
int probe_interval_telemetry = 120000;

//Every 1 minutes send sensors data to server
int last_probe_sensor = 0;
int probe_interval_sensor = 10000;

int last_bell = 0;
int bell_interval = 2000;
bool bellStart = false;
bool bellFirst = true;

RTC_DATA_ATTR int bootCount = 0;

void isConnectToWifi()
{
  WiFi.begin(WIFI_SSID, WIFI_PASS);
}

void isNtpUpdate()
{
  if (millis()>(current_state_ntp + refresh_interval_ntp))
  {
      timeClient.update();
      current_state_ntp = millis();
  }
}


void connectToMqtt()
{
  mqttClient.connect();
}

void WiFiEvent(WiFiEvent_t event)
{
  switch (event) {
  case SYSTEM_EVENT_STA_GOT_IP:
      connectToMqtt();
      break;
  case SYSTEM_EVENT_STA_DISCONNECTED:;
      xTimerStop(mqttReconnectTimer, 0); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
      xTimerStart(wifiReconnectTimer, 0);
      break;
  }
}

void onMqttConnect(bool sessionPresent)
{
  mqttClient.subscribe(REQUEST_SUB, 2);
  mqttClient.subscribe(RESPONSE_SUB, 2);
  mqttClient.subscribe(TELEMETRY_SUB, 0);

}

void isBell()
{
  if (bellStart)
  {
    if (bellFirst)
    {
      last_bell = millis();
      bellFirst = false;
      digitalWrite(buzzerPin, HIGH);
    }
    else if (millis()>(last_bell+bell_interval))
    {
      digitalWrite(buzzerPin, LOW);
      bellFirst = true;
      bellStart = false;
    }
  }
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason)
{
  if (WiFi.isConnected())
  {
    xTimerStart(mqttReconnectTimer, 0);
  }
}

void onMqttSubscribe(uint16_t packetId, uint8_t qos) {}
void onMqttUnsubscribe(uint16_t packetId) {}
void onMqttMessage(char* topic, char* payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index, size_t total) {
  //topic,properties.qos,properties.dup,properties.retain, len, index, total
  if (String(REQUEST_SUB).equals(String(topic)))
  {
    const size_t capacity = 2*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + 60;
    DynamicJsonDocument doc(capacity);

    deserializeJson(doc,payload);

    const char* device_id = doc["dev"]["id"];
    const char* device_type = doc["dev"]["type"];

		if (device_id == NODE && device_type == NODE_TYPE){
	    JsonObject request = doc["req"];
	    bool request_bell = request["bell"];
	    bool request_alarm = request["alarm"];
			bool request_reset = request["reset"];

	    if (request_bell)
	    {
	      bellStart = true;
	    }
	    else if (request_alarm)
	    {
	      digitalWrite(buzzerPin,HIGH);
	    }
			else if (request_reset)
			{
				ESP.restart();
			}
			else
			{
				digitalWrite(buzzerPin,LOW);
			}
		}

  }



}

void onMqttPublish(uint16_t packetId) {}

void isSendTelemetry()
{
  if (millis()>(last_probe_telemetry+probe_interval_telemetry))
  {
    const size_t capacity = 2*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(11);
    DynamicJsonDocument doc(capacity);

    JsonObject device = doc.createNestedObject("device");
    device["id"] = NODE;
    device["type"] = NODE_TYPE;

    JsonObject telemetry = doc.createNestedObject("telemetry");
    telemetry["ssid"] = WiFi.SSID();
		telemetry["rssi"] = String(WiFi.RSSI());
    telemetry["ip"] = WiFi.localIP().toString();
    telemetry["gateway"] =  WiFi.gatewayIP().toString();
    telemetry["temp"] = String((temprature_sens_read() - 32) / 1.8);
    telemetry["uptime"] = String(millis()/1000);
		telemetry["bootcount"] = String(bootCount);

		//TODO:
		//ESP.getSketchMD5()
		//ESP.getFlashChipSize()
		//ESP.getFlashChipSpeed()
		//ESP.getFreeHeap()
		//ESP.getHeapSize()
		//ESP.getMaxAllocHeap()
		//ESP.getMaxAllocPsram()
		//ESP.getSdkVersion()
		//ESP.getSketchSize()

		char send[5000];//IDEA: TE, ezért nem megy át az üzenet!!!!!!

    serializeJson(doc, send);
    mqttClient.publish("telemetry", 0, true, send);
    last_probe_telemetry = millis();
  }
}

void isSendSensor()
{
	if (millis()>(last_probe_sensor+probe_interval_sensor))
  {
		const size_t capacity = 2*JSON_OBJECT_SIZE(3);
		DynamicJsonDocument doc(capacity);

		JsonObject device = doc.createNestedObject("device");
		device["id"] = NODE;
		device["type"] = NODE_TYPE;

		JsonObject m = doc.createNestedObject("m");
		m["temp"] = ((temprature_sens_read() - 32) / 1.8);
		m["hum"] = 20;
		m["air"] = 10;

		char send[5000];//IDEA: TE, ezért nem megy át az üzenet!!!!!!

    serializeJson(doc, send);
    mqttClient.publish("sensor", 0, true, send);

    last_probe_sensor = millis();
  }
}


void setup(){
	++bootCount;
  if (debug)
  {
      Serial.begin(9600);
  }
  pinMode(buzzerPin, OUTPUT);


  mqttReconnectTimer = xTimerCreate("mqttTimer", pdMS_TO_TICKS(2000), pdFALSE, (void*)0, reinterpret_cast<TimerCallbackFunction_t>(connectToMqtt));
  wifiReconnectTimer = xTimerCreate("wifiTimer", pdMS_TO_TICKS(2000), pdFALSE, (void*)0, reinterpret_cast<TimerCallbackFunction_t>(isConnectToWifi));

  WiFi.onEvent(WiFiEvent);
	delay(3000);
	isConnectToWifi();

  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onSubscribe(onMqttSubscribe);
  mqttClient.onUnsubscribe(onMqttUnsubscribe);
  mqttClient.onMessage(onMqttMessage);
  mqttClient.onPublish(onMqttPublish);
  mqttClient.setServer(IPAddress(144, 91, 66, 2), MQTT_PORT);
  mqttClient.setCredentials(MQTT_USER, MQTT_PASS);
  mqttClient.setMaxTopicLength(2048); //Ha gond lenne az topic hosszával




  //ello message
  mqttClient.subscribe("ello", 2);
  mqttClient.publish("ello", 2, true, NODE);
  mqttClient.unsubscribe("ello");

  timeClient.begin();
  timeClient.update();

	Serial.println("-------------------------------");
	Serial.println("Wifi SSID: " + WiFi.SSID());
	Serial.println("Wifi IP: "+ WiFi.localIP());
	Serial.println("Wifi GTW: "+ WiFi.gatewayIP());
	Serial.println("Wifi DNS IP: " + WiFi.dnsIP());
	Serial.println("-------------------------------");
}
void loop()
{
  isSendTelemetry();
  isBell();
	isSendSensor();
}
