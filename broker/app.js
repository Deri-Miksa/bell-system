var mosca = require('mosca');
require('dotenv').config()
const InitiateMongoServer = require("./db");
// Initiate Mongo Server
InitiateMongoServer();

const Today = require("./models/today");
const Telemetry = require("./models/telemetry");

var port = normalizePort(process.env.PORT || '1883');

var settings = {
  port: port
};

var server = new mosca.Server(settings);

server.on('clientConnected', function(client) {
    console.log('client connected: ', client.id);
});

// fired when a message is received
server.on('published', async function(packet, client) {
  switch (packet.topic) {
    case "sensor":
      try {

        var data = JSON.parse(packet.payload);

        var temperature = data.m.temp;
        var humidity = data.m.hum;
        var airquality = data.m.air;
        var id = data.device.id;
        var type = data.device.type;
        var date = Date.now();

        today = new Today({
          temperature,
          humidity,
          airquality,
          id,
          type,
          date
        });

        await today.save();

      } catch (e) {
        console.log("Something broke: ", e)
      }

      break;
    case "telemetry":
      try {

        var data = JSON.parse(packet.payload);

        var id = data.device.id;
        var type = data.device.type;
        var ssid = data.telemetry.ssid;
        var rssi = data.telemetry.rssi;
        var ip = data.telemetry.ip;
        var gateway = data.telemetry.gateway;
        var temperature = data.telemetry.temp;
        var uptime = data.telemetry.uptime;
        var bootcount = data.telemetry.bootcount;
        var date = Date.now();

        telemetry = new Telemetry({
          ssid,
          rssi,
          ip,
          gateway,
          temperature,
          uptime,
          bootcount,
          id,
          type,
          date
        });
        await telemetry.save();
      } catch (e) {
        console.log("Something broke: ", e)
      }
      break;
  }
});

server.on('ready', setup);

var authenticate = function(client, username, password, callback) {
  var authorized = (username === 'deri' && password.toString() === 'miksa');
  if (authorized) client.user = username;
  callback(null, authorized);
}

// fired when the mqtt server is ready
function setup() {
  server.authenticate = authenticate;
  console.log('Mosca server is up and running');
}

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
