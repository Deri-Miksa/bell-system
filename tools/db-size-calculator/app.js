const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

//console.log(`${name}, is a citizen of ${country}`);

rl.question("What is the durration(in year)? ", function(year) {
    rl.question("How many devices do you want to use? ", function(devices) {
      rl.question("How often is the measurement(In one minute)? ", function(period) {
        rl.question("What is the average data size(in Byte)? ", function(size) {


          var one = (((year*12*30*24*60*period)*size)/1024)/1024;
          console.log("\nWithout optimalization:");
          console.log("------------------------------");
          console.log('When one device:',one,'MB');
          console.log('When',devices,'devices: ', one*devices, 'MB');

          console.log("\nWith optimalization:");
          console.log("------------------------------");
          console.log('When one device:',((12*1*size*year)/1024)/1024,'MB');
          console.log('When',devices,'devices: ', ((12*1*size*year*devices)/1024)/1024, 'MB');

          rl.close();
        });
      });
    });
});

rl.on("close", function() {
    console.log("\n---");
    process.exit(0);
});
