const mqtt = require('mqtt')
const client = mqtt.connect('mqtt://144.91.66.2', {username: "deri", password: "miksa"})

const MESSAGE_NUMBER = 10000;
const REPEAT = 10000;
const DELAY = 1000;

client.on('connect', async function () {
  client.subscribe('sensor', async function (err) {
    if (!err) {
      for (var j = 0; j < REPEAT; j++) {
          await sleep(DELAY);
        for (var i = 0; i <= MESSAGE_NUMBER; i++) {
          process.stdout.cursorTo(0);
          client.publish('sensor', '{"device":{"id":' + Math.floor(Math.random() * Math.floor(200)) + ',"type":"ESP32"},"m":{"temp":' + Math.floor(Math.random() * Math.floor(40)) + ',"hum":' + Math.floor(Math.random() * Math.floor(100)) +',"air":'+ Math.floor(Math.random() * Math.floor(100)) +'}}');
          process.stdout.write("MESSAGE: " + i);
        }
      }
    }
  })
})

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
